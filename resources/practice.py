# Python program to illustrate
# *args for variable number of arguments
def myFun(*argv):
    #for arg in argv:
    print(argv)
#myFun('Hello', 'Welcome', 'to', 'GeeksforGeeks')

def myFun(**kwargs):
    print(kwargs)
    for key, value in kwargs.items():
        print("%s == %s" % (key, value))
myFun(first='Geeks', mid='for', last='Geeks')



months = ('January','February','July','August','September','October','November','  December')
#python list
# List is a collection which is ordered and changeable. Allows duplicate members.
list = [1, 2, 3, 4, 5, 6, 7 ]
#print(list[1])

#Tuple is a collection which is ordered and unchangeable. Allows duplicate members.
thistuple = ("apple", "banana", "cherry")
#print(thistuple[1])

#Dictionary is a collection which is unordered, changeable and indexed. No duplicate members.
thisdict = {
  "brand": "Ford",
  "brand": "ok",
  "year": 1964
}
#print(thisdict)