import datetime
import uuid

from django.db import models
from django.utils import timezone

from projectzero.res_manager import get_resource
from projectzero.validators import *
from resources.strings import SUBJECT_UNIQUE_ERROR


# Create your models here.
class Subject(models.Model):
    subject_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    subject_code = models.CharField(max_length=10, unique=True, null=False,
                                    validators=[check_valid_chars, MinLengthValidator(3)],
                                    error_messages={'unique': SUBJECT_UNIQUE_ERROR},
                                    verbose_name=get_resource('SUBJECT_CODE'))
    subject_text = models.CharField(max_length=100, null=False,
                                    verbose_name=get_resource('SUBJECT_TEXT'))
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.subject_text

    def __init__(self, *args, **kwargs):
        super(Subject, self).__init__( *args, **kwargs)
        self._meta.get_field('subject_code').verbose_name = get_resource('SUBJECT_CODE')
        self._meta.get_field('subject_text').verbose_name = get_resource('SUBJECT_TEXT')

    class Meta:
        verbose_name = get_resource('SUBJECT_TITLE_SINGULAR')
        verbose_name_plural = get_resource('SUBJECT_TITLE_PLURAL')

        

class Topic(models.Model):
    topic_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    topic_code = models.CharField(null=False, validators=[check_valid_chars, MinLengthValidator(4)], max_length=10)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='subject')
    topic_text = models.CharField(max_length=250, )
    created_date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.topic_text

    class Meta:
        verbose_name_plural = get_resource('TOPIC_TITLE_PLURAL')

    def was_published_recently(self):
        return self.created_date >= timezone.now() - datetime.timedelta(days=1)

