from django.forms import Form, BaseModelForm, ModelForm, forms
from django.utils.functional import lazy
from django.utils.translation import gettext_lazy

from projectzero.res_manager import get_resource
from question_category.models import Subject


class SubjectForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(SubjectForm, self).__init__(*args, **kwargs)
        self.fields.get('subject_code').label = get_resource('SUBJECT_CODE')
        self.fields.get('subject_text').label = get_resource('SUBJECT_TEXT')


