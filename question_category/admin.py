from django.contrib import admin

# Register your models here.
from django.db import models
from django.template.response import TemplateResponse

from question_category.forms import SubjectForm
from django.urls import path
from django.http import HttpResponse
from django.contrib.admin import AdminSite
from question_category.models import Subject, Topic
from question_category.views import *


class SubjectAdmin(admin.ModelAdmin):
    form = SubjectForm
    list_display = ['subject_code', 'subject_text']
    save_on_top = True

class TopicAdmin(admin.ModelAdmin):
    model = Topic
    list_display = ['topic_code', 'subject_id', 'topic_text']

class ReportAdmin(admin.ModelAdmin):
    change_form_template = 'question_category/subject-list.html'
    list_display = ['topic_code', 'subject_id', 'topic_text']


class DummyModel(models.Model):
    class Meta:
       managed = False


#@admin.register(DummyModel)
class DummyModelAdmin(admin.ModelAdmin):
    # model = DummyModel

    def get_urls(self):
        view_name = '{}_{}_changelist'.format(
            self.model._meta.app_label, self.model._meta.model_name)
        return [
            #path('my_admin_path/', self.admin_site.admin_view(my_custom_view)), ## For admin user only
            path('subjectlist', SubjectListView.as_view(), name='index'),
            #path('subjectlist/', subjectlist, name=view_name), ## for public user only
        ]

#admin.site.register(DummyModel, DummyModelAdmin)
class MyModelAdmin(admin.ModelAdmin):
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('my_view/', subjectlist),
        ]
        return my_urls + urls
    def my_view(self, request):
        # ...
        context = dict(
            # Include common variables for rendering the admin template.
            self.admin_site.each_context(request),
            # Anything else you want in the context...

        )
        return TemplateResponse(request, "question_category/subject-list.html", context)

admin.site.register(DummyModel, DummyModelAdmin)

admin.site.register(Subject, SubjectAdmin)
admin.site.register(Topic, TopicAdmin)