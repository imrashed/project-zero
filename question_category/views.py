from django.http import HttpResponse
from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views import generic
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from django.views.generic import ListView

from question.models import Question
from question_category.models import Subject
from rest_framework import generics

from question_category.tables import SubjectTable


@method_decorator(login_required, name='dispatch')
class IndexView(generic.ListView):
    template_name = 'question_category/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Subject.objects.order_by('-subject_id')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'question_category/detail.html'

#@method_decorator(login_required, name='dispatch')
def subjectlist(request):
    for i in range(600):
        sub = Subject()
        sub.subject_code = i
        sub.subject_text = 'test'+ str(i)
        sub.save()

    subjects = Subject.objects.all()
    context = {
        'subjects' : subjects,
    }
    return render(request, 'question_category/subject-list.html', context)

class SubjectListView(SingleTableMixin, FilterView):
    model = Subject
    table_class = SubjectTable
    template_name = 'question_category/subject.html'