from django.urls import path

from . import views


urlpatterns = [
    path('list', views.SubjectViewSet.as_view(), name='subject'),

]