from rest_framework import generics

from question_category.api.serializers import SubjectSerializer
from question_category.models import Subject


class SubjectViewSet(generics.CreateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Subject.objects.all().order_by('subject_id')
    serializer_class = SubjectSerializer