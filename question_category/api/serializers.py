from rest_framework import serializers

from question_category.models import Subject


class SubjectSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Subject
        fields = ['subject_code', 'subject_text']

