from django.core.exceptions import ValidationError
from django.core.validators import BaseValidator

from resources.strings import DEFAULT_MIN_LENGTH_ERROR


def check_valid_chars(value):
    if '_' in value :
        raise ValidationError('Invalid code')

class MinLengthValidator(BaseValidator):
    def compare(self, a, b):
        if len(str(a)) < b :
            raise ValidationError(DEFAULT_MIN_LENGTH_ERROR.format(b))