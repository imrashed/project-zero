import django_tables2 as tables
from .models import Subject

class SubjectTable(tables.Table):
    class Meta:
        model = Subject
        template_name = "django_tables2/bootstrap.html"
        fields = ("subject_code","subject_text")