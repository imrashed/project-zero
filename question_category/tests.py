from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.test import TestCase
from django.utils import timezone
from projectzero.validators import check_valid_chars
from .models import Topic, Subject



class SubjectTests(TestCase):
    def test__when_null_code__should_raise_error(self):
        s = Subject(subject_text="test", created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_empty_code__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_less_3_char_code__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_10_plus_char_code__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A1234567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__dash_code__should_pass(self):
        time = timezone.now()
        s = Subject(subject_text="test", subject_code='AA-', created_date=time)
        try:
            s.full_clean()
        except:
            self.fail()

    def test__when_underscore__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A1234567890_', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()



    def test__when_space__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123456 7890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_questionsign__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345?67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_comma__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345,67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_curly_bracket_first__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12{34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_curly_bracket_last__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12}34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_period__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12.34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_plus__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12+34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_single_quote__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12\'34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_double_quote__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12"34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_back_slash__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12\\34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_equal__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12=34567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_atsign__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123@4567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_exclamation_mark__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345!67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_hash__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123#45!67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_dollarsymbol__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345$67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_slash__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A1234/5!67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_first_bracket_first__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345(67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_percent__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345%67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_square_bracket_last__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A1234567]890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_square_bracket_fast__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345[67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_less_then__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123<4567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_greater_then__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345>67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_ampersand_should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123456&7890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_star__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A12345*67890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_caret__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123^4567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__when_semicolon__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123:4567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()
    def test__when_tild__should_raise_error(self):
        s = Subject(subject_text="test", subject_code='A123~4567890', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    ## Subject Unique validation
    def test__when_subject_code_duplicate__should_raise_error(self):
        s = Subject(subject_code='test',subject_text='another text',created_date=timezone.now())
        s1 = Subject(subject_code='test',subject_text='text',created_date=timezone.now())
        with self.assertRaises(IntegrityError):
            s.save()
            s1.save()


    #------------------------------------------------------------------------------------------#
    # Subject text unit testing start from here
    def test__null_text__should_raise_error(self):
        s = Subject(subject_code="test", created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__empty_text__should_raise_error(self):
        s = Subject(subject_text="", subject_code='AAA', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__less_3_char_text__should_raise_error(self):
        s = Subject(subject_text="te", subject_code='A', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__100_plus_char_text__should_raise_error(self):
        s = Subject(subject_text="tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft tesg sdfgsdg sgddft ",
                    subject_code='A690', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()




class TopicTests(TestCase):
    def setUp(self):
        s1 = Subject(subject_code='test', subject_text='test', created_date=timezone.now())
        s1.save()
        self.subject = s1

    ## Topic code testing start
    def test__null_code__should_raise_error(self):
        s = Topic(subject_id = self.subject, topic_text="test", created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__less_3_char_code__should_raise_error(self):
        s = Topic(subject_id = self.subject, topic_text="te", topic_code='A', created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__max_length_validation_added(self):
        max_length = Topic._meta.get_field('topic_code').max_length
        self.assertEquals(max_length, 10)

    def test__char_validation_added(self):
        validators = Topic._meta.get_field('topic_code').validators
        self.assertIn(check_valid_chars, validators)

    ## Topic subject testing start
    def test__null_subject__should_raise_error(self):
        s = Topic(topic_text="test", created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()
