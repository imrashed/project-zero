from django.apps import AppConfig


class QuestionCategoryConfig(AppConfig):
    name = 'question_category'
