from django.test import TestCase
from django.utils import timezone
from django.core.exceptions import ValidationError
from django.db import IntegrityError
from projectzero.validators import *

# Create your tests here.
from question.models import Question
from question_category.models import Subject, Topic


class QuestionTest(TestCase):
    def setUp(self):
        self.s1 = Subject(subject_code='test', subject_text='test', created_date=timezone.now())
        self.s1.save()

        self.t1 = Topic(subject=self.s1, topic_code='test', topic_text='test', created_date=timezone.now())
        self.t1.save()


    ## Question code testing start
    def test__null_code__should_raise_error(self):
        s = Question(question_text="test", question_type="radio",
                     subject=self.s1, topic=self.t1, is_draft=False, created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__less_3_char_code__should_raise_error(self):
        s = Question(question_code="st", question_text="test", question_type="radio",
                     subject=self.s1, topic=self.t1, is_draft=False, created_date=timezone.now())
        with self.assertRaises(ValidationError):
            s.full_clean()

    def test__max_length_validation_added(self):
        max_length = Topic._meta.get_field('question_code').max_length
        self.assertEquals(max_length, 10)

    def test__char_validation_added(self):
        validators = Question._meta.get_field('question_code').validators
        self.assertIn(check_valid_chars, validators)

    ## Question code Unique validation
    def test__when_question_code_duplicate__should_raise_error(self):
        s = Question(question_code="st", question_text="test", question_type="radio",
                     subject=self.s1, topic=self.t1, is_draft=False, created_date=timezone.now())
        s1 = Question(question_code="st", question_text="test", question_type="radio",
                     subject=self.s1, topic=self.t1, is_draft=False, created_date=timezone.now())
        with self.assertRaises(IntegrityError):
            s.save()
            s1.save()

class QuetionOptionTest:
    pass