from django.contrib import admin
from .models import Question, QuestionOption

class QuestionOptionInline(admin.TabularInline):
    model = QuestionOption
    extra = 2

class QuestionAdmin(admin.ModelAdmin):
    inlines = [QuestionOptionInline]

# Register your models here.
admin.site.register(Question, QuestionAdmin)