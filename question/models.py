from datetime import datetime
from django.db import models
from django.contrib.auth.models import AbstractUser


class Question(models.Model):
    question_text = models.TextField()
    question_id = models.CharField(max_length=200, default='null')

    def __str__(self):
        return self.question_text

    class Meta:
        verbose_name_plural = "Questions"



class QuestionOption(models.Model):
    option_text = models.CharField(max_length=1000)
    question = models.ForeignKey(Question, related_name='question_option', on_delete=models.CASCADE)
    correct = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%d: %s' % (self.id, self.option_text)

    class Meta:
        verbose_name_plural = "Questions Options"
