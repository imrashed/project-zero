import os

from projectzero.settings_dev import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS=['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'projectzero',
        'USER': 'root',
        'PASSWORD': '@ish123#',
        'HOST': 'projectzero-db',
        'PORT': '3306',
        'OPTIONS': {
            'sql_mode': 'traditional',
        }
    }
}

RQ_QUEUES = {
    'default': {
        'HOST': 'projectzero-redis',
        'PORT': 6379,
        'DB': 0,
        'PASSWORD': '',
        'DEFAULT_TIMEOUT': 360,
    }
}


