from django.http import HttpResponse
from projectzero import res_manager
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from rest_framework import viewsets
from projectzero.serializers import UserSerializer, GroupSerializer

def lang(request,lang):
    res_manager.language = lang
    return HttpResponse(lang)

class UserViewSet(viewsets.ModelViewSet):
    User = get_user_model()
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer