import pandas as pd
from pandas import DataFrame
from termcolor import cprint

string_mui = DataFrame()
language = 'en-us'


def get_resource(item):
    global language
    global string_mui
    try:
        if string_mui.empty :
            string_mui = pd.read_csv('resources/string.csv', index_col=0)
        return string_mui[language][item]
    except Exception as ex:
        cprint(ex, 'red')
        return '-'