This project contains following:
Subjects
Topics
Questions
QuestionOptions

-- For each module there will be seperate 'django app', not for each model
1. Subject and Topic > question_category
2. Question and QuestionOption > question

Subject:
--------
subject_id PK, UUID
subject_code NOTNULL, UNIQUE, MIN 3Ch, MAX 10Ch, Only Alphanumeric and '-'
subject_text NOTNULL, MIN 3Ch, MAX 100Ch

Topic:
------
topic_id PK, UUID
topic_code NOTNULL, UNIQUE with subject, MIN 3Ch, MAX 10Ch, Only Alphanumeric and '-'
topic_text NOTNULL, MIN 3Ch, MAX 100Ch
subject FK, NOTNULL

Question:
---------
question_id PK, UUID
question_code NULL, UNIQUE, MIN 3Ch, MAX 10Ch, Only Alphanumeric
question_text NOTNULL, MIN 3Ch, MAX 1000Ch
question_type NOTNULL
subject FK, NOTNULL
topic FK, NOTNULL
is_draft NOTNULL, boolean
answer_keys Concatetaion of answers in question option, update on edit
weight NOTNULL, NUMERIC(2,1), DEFAULT 1

QuestionOption:
---------------
option_id PK, UUID
option_code NOTNULL, UNIQUE with Question, MIN 1Ch, MAX 1Ch, Only Alphanumeric
sort_order NOTNULL, UNIQUE, INTEGER
option_text NOTNULL, MIN 3Ch, MAX 1000Ch
is_answer NOTNULL, Boolean
question FK, NOTNULL

All input will be trimmed for whitespace on both side
If question is not draft, there must be at least two options, and one of these must be correct at least
There will be maximum 6 options of a question.





