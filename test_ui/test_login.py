import time

from selenium.common.exceptions import NoSuchElementException

from test_ui.config import *


def login(driver, data):
   time.sleep(DELAY_LONG)
   admin_name = driver.find_element_by_name('username')
   admin_name.clear()
   admin_name.send_keys(data['username'])
   admin_pass = driver.find_element_by_name('password')
   admin_pass.send_keys(data['password'])
   admin_pass.submit()
   try:
      time.sleep(DELAY_LONG)
      driver.find_element_by_xpath('/html/body/div/div/div/div[1]/div[2]/a[3]')
      return 1
   except NoSuchElementException:
      return 0



def logout(driver):
   logout_link = driver.find_element_by_xpath('/html/body/div/div/div/div[1]/div[2]/a[3]')
   logout_link.click()

