import time

import pandas as pd
from selenium.common.exceptions import NoSuchElementException

from test_ui.config import *

def addSubject(driver, data):
      if not pd.isna(data['_subjectcode']):
         subject_code = driver.find_element_by_name('subject_code')
         subject_code.send_keys(data['_subjectcode'])

      if not pd.isna(data['_subjectname']):
         subject_name = driver.find_element_by_name('subject_text')
         subject_name.send_keys(data['_subjectname'])

      save_button = driver.find_element_by_name('_save')
      save_button.click()


      try:
         time.sleep(DELAY_LONG)
         driver.find_element_by_link_text(data['_subjectname'])
         return 1
      except NoSuchElementException:
         return 0

