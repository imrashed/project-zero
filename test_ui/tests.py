import unittest

import pandas as pd
from django.utils.timezone import now
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from test_ui.config import *
from test_ui.test_login import login, logout
from test_ui.test_subject import addSubject


class TestUI(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        if HIDE_CHROME_DRIVER:
            chrome_options.add_argument("--headless")
        self.driver = webdriver.Chrome(CHROME_DRIVER_LOCATION, chrome_options=chrome_options)
        self.driver.get(MAIN_URL)
        self.driver.maximize_window()

    def testLogin(self):
        data = pd.read_csv("test_ui/testdata/login.csv", dtype=str)
        for idx, row in data.iterrows():
            self.driver.get(MAIN_URL)
            actual = login(self.driver, row)
            self.assertEqual( row['expected'], str(actual))
            if actual == 1: logout(self.driver)

    # def testSubject(self):
    #     login(self.driver, {'username':'admin', 'password': '123', 'name': 'Admin'})
    #     data = pd.read_csv("test_ui/testdata/addsubject.csv", dtype=str)
    #     for idx, row in data.iterrows():
    #         self.driver.get(MAIN_URL + ADD_SUB_URL)
    #         actual = addSubject(self.driver, row)
    #         self.assertEqual( row['expected'], str(actual))


    def testSubjectAdd(self):
        login(self.driver, {'username':'admin', 'password': '123', 'name': 'Admin'})
        data = pd.read_csv("test_ui/testdata/subject.csv", dtype=str)
        for idx, row in data.iterrows():
            self.driver.get(MAIN_URL + ADD_SUB_URL)
            actual = addSubject(self.driver, row)
            data['actual_result']=actual
            data['execution_time'] = now()
            data['status'] = (row['expected_result'] == str(actual))
            #self.assertEqual( row['expected_result'], str(actual))

        data.to_csv("test_ui/testresult/subject_result.csv", index=False)


    # def testSchool(self):
    #     login(self.driver, {'email':'admin', 'password': '123', 'name': 'Admin'})
    #     data = pd.read_csv("test_ui/testdata/school.csv", dtype=str)
    #     for idx, row in data.iterrows():
    #         actual = addSchool(self.driver, row)
    #         self.assertEqual( row['expected'], str(actual))
